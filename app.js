var amqp = require('amqp'),
    redis = require("redis"),
    sockjs = require('sockjs'),
    static = require('node-static'),
    http = require('http'),
    Report = require('./report');

var arguments = process.argv.splice(2);
var fakeHandler = ((arguments.indexOf('-f') != -1) ||  (arguments.indexOf('--fake') != -1));
var node_env = (process.env.NODE_ENV)?process.env.NODE_ENV:'development'; // or 'production' or 'test'

var config = require('./config/'+node_env);

var redisDB = redis.createClient(config.redis.port, config.redis.host, config.redis.options);
if( config.redis.password ){
  redisDB.auth(config.redis.password, function( error ){
    if( error ){
      console.log("Report is Stoping", "Error: "+error);
      process.exit(1);
    };
  });
}

//Report
var report = new Report(config.report, redisDB);

//HTTP Server
var serverStatic = new static.Server('./public', {cache: false});
var serverHttp = http.createServer(function (request, response) {
    request.addListener('end', function () {
        serverStatic.serve(request, response);
    }).resume();
}).listen(3000, '0.0.0.0');

//Web Socket
var serverSocket = sockjs.createServer();
serverSocket.on('connection', function( conn ) {
  report.on(Report.events.remove, function ( id ) {
    conn.write(JSON.stringify({cmd:'REMOVE', obj:id}));
  });
  report.on(Report.events.add, function ( obj ) {
    conn.write(JSON.stringify({cmd:'ADD', obj:obj}));
  });
  report.on(Report.events.interval, function ( obj ) {
    conn.write(JSON.stringify({cmd:'INTERVAl', obj:obj}));
  });
  conn.on('data', function( message ) {
    var data = JSON.parse(message);
    switch( data.cmd ){
      case 'OLD_TRUNKS':
        conn.write(JSON.stringify({cmd:'OLD_TRUNKS', obj:report.times}));
      break;
    }
  });
  conn.write(JSON.stringify({cmd:'CONFIG', obj:config.report}));
  conn.write(JSON.stringify({cmd:'INIT', obj:report.current}));
  conn.on('close', function() {
    
  });
});
serverSocket.installHandlers(serverHttp, {prefix:'/sockjs'});


report.on(Report.events.error, function ( error ) {
  console.log("Report is Stoping", "Error: "+error);
  process.exit(1);
});

report.on(Report.events.reset, function ( ) {
  console.log("Report is Reseting");
});

report.on(Report.events.ready, function () {
  var connection = amqp.createConnection( config.amq.connection );
  
  connection.on('error', function ( error ) {
    console.log("Rabbit has "+error);
    process.exit(1);
  });

  connection.on('ready', function () {

    connection.queue(config.amq.queue.name, config.amq.queue.options, function( queue ){
      queue.bind(config.amq.routing_key);
      queue.subscribe(function ( message ) {
        report.receiveAMQMessage( message );
      });
    });
    
    if( fakeHandler ){
      var Trunk = require('./models/trunk');

      //Only for test
      function createAMQMessage( trunk ){
        var now = new Date();
        time = Math.floor(now.getTime()/1000);
        return {
          id_trunk: trunk.id_trunk,
          active: trunk.active,
          group: trunk.group,
          time: trunk.time
        };
      };

      console.log("Handler Fake is active.");
      connection.exchange(config.amq.exchange, {}, function( exchange ){
        for(var i = 0; i < 1000; i++ ){
          var groups = ['Group 1', 'Group 2'];
          var fakeTrunk = new Trunk((i+1), groups[(i%2)], false, 0);
          setInterval(function(fakeTrunk){
              fakeTrunk.active = !fakeTrunk.active;
              exchange.publish(config.amq.routing_key, JSON.stringify(createAMQMessage(fakeTrunk)), {contentType: 'application/json'});
          }, (Math.random()*1000)+1000, fakeTrunk);
        }
      });
    }

  });
});