var events = require('events'),
	util = require('util'),
	Trunk = require('./models/trunk');

function Report( config, redis, mysql ){
	this.prefix_redis = 'ReportTrunk';
	this.config = config;
	this.redis = redis;
	this.mysql = mysql;// wil be used for groups
	this.set_interval = null;
	this.callback_trunks = [];
	this.current = [];
	this.times = [];
	for( var i = 0; i < config.length; i++) {
		this.times.push([]);
	}
	this._addListiners();
};

// Report Events
Report.events = {
	error: 'onError',
	ready: 'onReady',
	interval: 'onInterval',
	reset: 'onReset',
	remove: 'onRemove',
	add: 'onAdd'
};

Report.prototype = {
	//Public
	reset: function(){
		this.emit(Report.events.reset);
		this.redis.KEYS(this._returnRedisKeyName('*'), this._callbackRedis(function( keys ){
			this.redis.DEL(keys, this._callbackRedis(function( deleted ){
				this._createConfigRedis();
			}));
		}));
	},
	receiveAMQMessage: function( message ){
		var trunk_current = this._getTrunkByAMQMessage( message );
		if( trunk_current.time > this.timeLimit() ){
			var key_redis = this._returnRedisKeyName('trunk', trunk_current.time, trunk_current.id_trunk );
			var timeOfLife = trunk_current.time - this.timeLimit();
			if( trunk_current.active ){
				this.current.push(trunk_current);
				this.emit(Report.events.add, trunk_current);
				this.redis.HMSET(key_redis, trunk_current.toRedis(), this._callbackRedis(function( deleted ){
					this.redis.EXPIRE(key_redis, timeOfLife, this._callbackRedis(function( expired ){}));
				}));
			} else {
				var $this = this;
				for( var i = 0; i < this.current.length; i++ ){
					var trunk = this.current[i];
					if( trunk_current.id_trunk == trunk.id_trunk ){
						var indice = this.current.indexOf(trunk);
						this.current.splice(indice, 1);
						$this.emit(Report.events.remove, indice);
						$this.redis.DEL(key_redis, $this._callbackRedis(function( deleted ){}));
						break;
					}
				}
			}
		}
	},
	timeLimit: function(){
		var now = new Date();
		var time = Math.floor(now.getTime()/1000);
		return (time-(this.config.interval*this.config.length));
	},
	getTrunksByTime: function( time ){
		var currentTime = Math.floor(parseInt(time)/this.config.interval)%this.config.length;
		return this.times[currentTime];
	},
	//Private
	_addListiners: function(){
		var $this = this;
		this.redis.on("error", function ( error ) {
		  $this.emit(Report.events.error, "Is not possible connect with Redis: "+error);
		});
		this.redis.on("ready", function () {
			$this._getConfigRedis();
		});
	},
	_callbackRedis: function( callback ){
		var $this = this;
		return function(){
			var args = Array.prototype.slice.call(arguments);
			var error = args[0];
			var arg = args.splice(1);
			if( error ){
				$this.emit(Report.events.error, "Redis have a "+error.toString());
			} else {
				callback.apply($this, arg);
			}
		};
	},
	_returnRedisKeyName: function(){
		var args = Array.prototype.slice.call(arguments);
		args.unshift(this.prefix_redis);
		return args.join('.');
	},
	_getConfigRedis: function(){
		this.redis.EXISTS(this._returnRedisKeyName('config'), this._callbackRedis(function( exist ){
			if( exist ){
				this.redis.HGETALL(this._returnRedisKeyName('config'), this._callbackRedis(function( redisConfig ){
					var equal = ((redisConfig.interval == this.config.interval.toString()) && (redisConfig.length == this.config.length.toString()));
					if( !equal ){
						this.reset();
					} else {
						this._getDataOfRedis();
					}
				}));
			} else {
				this._createConfigRedis();
			}
		}));
	},
	_createConfigRedis: function(){
		var redisConfig = {
			interval: this.config.interval.toString(),
			length:this.config.length.toString()
		};
		this.redis.HMSET(this._returnRedisKeyName('config'), redisConfig, this._callbackRedis(function(){
			this._init();
		}));
	},
	_getDataOfRedis: function(){
		this.redis.KEYS(this._returnRedisKeyName('trunk', '*'), this._callbackRedis(function( keys ){
			for( var i = 0; i < keys.length; i++ ){
				var key = keys[i];
				var info = key.split('.');
				if( info[2] > this.timeLimit() ){
					this.callback_trunks.push(key);
				}
			}
			if( this.callback_trunks.length ){
				this._getTrunksCallBack();
			} else {
				this._init();
			}
		}));
	},
	_getTrunksCallBack: function(){
		if( this.callback_trunks.length ){
			var key = this.callback_trunks.splice(0, 1);
			this.redis.HGETALL(key[0], this._callbackRedis(function( trunk ){
				var trunkCallBack = Trunk.fromRedis(trunk);
				var timeTrunk = this.getTrunksByTime(trunkCallBack.time);
				timeTrunk.push(trunkCallBack);
				this._getTrunksCallBack();
			}));
		} else {
			this._init();
		}
	},
	_init: function(){
		var $this = this;
		this.set_interval = setInterval(function(){
			$this._clearLastTimeOfTrunks();
			var now = new Date();
			var time = Math.floor(now.getTime()/1000);
			$this.emit(Report.events.interval, time);
		}, this.config.interval*1000);
		this.emit(Report.events.ready);
	},
	_getTrunkByAMQMessage: function( message ){
		var now = new Date();
		var time = Math.floor(now.getTime()/1000);
		var trunk = new Trunk(message.id_trunk, message.group, message.active, time);
		return trunk;
	},
	_clearLastTimeOfTrunks: function(){
		this.times.splice(0, 1);
		this.times.push(this.current.slice());
	}
}
Report.prototype.__proto__ = events.EventEmitter.prototype;

module.exports = Report;