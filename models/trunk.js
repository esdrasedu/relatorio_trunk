function Trunk( id_trunk, group, active, time ){
	this.id_trunk = id_trunk;
	this.group = group;
	this.active = active;
	this.time = time;
};
Trunk.prototype = {
	toRedis: function(){
		return {
			id_trunk: this.id_trunk.toString(),
			group: this.group,
			time: this.time.toString(),
		};
	}
};
Trunk.fromRedis = function( redis ){
	return new Trunk(
		parseInt(redis.id_trunk),
		redis.group,
		true,
		parseInt(redis.time)
	);
};
//Trunk.prototype.__proto__ = events.EventEmitter.prototype;
module.exports = Trunk;