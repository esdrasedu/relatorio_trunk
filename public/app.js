angular.module('ReportTrunk', [])
.controller('CurrentTrunksCtrl', function( $scope, $rootScope ) {
	
	$scope.groups = [];
	$scope.groupsName = [];
	
	$scope.update = function(){
		for( var i = 0; i < $scope.groups.length; i++ ){
			$scope.groups[i].total = 0;
		}
		if( $rootScope.trunks && $rootScope.trunks.length ){
			for( var i = 0; i < $rootScope.trunks.length; i++ ){
				var trunk = $rootScope.trunks[i];
				var indice = $scope.groupsName.indexOf(trunk.group);
				if( indice == -1 ){
					indice = $scope.groupsName.length;
					$scope.groupsName.push(trunk.group);
					$scope.groups.push({name:trunk.group, total:0});
				}
				$scope.groups[indice].total++;
			}
		}
	};
	$scope.$on('SockJS.onMessage', function( event, cmd, arg ){
		switch( cmd ){
			case 'INIT':
				$scope.update();
			break; 
			case 'ADD':
				$scope.update();
			break;
			case 'REMOVE':
				$scope.update();
			break;
		}
	});

})
.controller('ReportTrunksCtrl', function( $scope, $rootScope ) {

	$scope.groups = [];
	$scope.groupsName = [];
	
	$scope.chart = null;

	$scope.init = {
		chart: { type: 'spline' },
		title: { text: 'Relatório de Trunk' },
		xAxis: {
		    type: 'datetime',
		    tickPixelInterval: 150
		},
		yAxis: {
		    title: { text: 'Trunks' }
		},
		tooltip: {
		    formatter: function() {
		        return '<b>'+ this.series.name +'</b><br/>'+
		        Highcharts.dateFormat('%d/%m/%Y %H:%M:%S', this.x) +'<br/>'+
		        Highcharts.numberFormat(this.y, 2);
		    }
		},
		legend: { enabled: false },
		exporting: { enabled: false }
	};

	$scope.$on('SockJS.onMessage', function( event, cmd, arg ){
		switch( cmd ){
			
			case 'INIT':
				$rootScope.sockjs.send(angular.toJson({cmd:"OLD_TRUNKS"}));
			break;
			case 'OLD_TRUNKS':
				var dataTime = {};
				var now = new Date();
				var timeNow = Math.floor(now.getTime()/1000);
				for(var i = 0; i < arg.length; i++){
					var time = arg[i];
					var timeCurrent = timeNow-((arg.length-i)*$rootScope.config.interval);
					for( var t = 0; t < time.length; t++ ){
						var trunk = time[t];
						var indice = $scope.groupsName.indexOf(trunk.group);
						if( indice == -1 ){
							indice = $scope.groupsName.length;
							$scope.groupsName.push(trunk.group);
							$scope.groups.push({name:trunk.group, total:0, serie:null});
						}
						$scope.groups[indice].total++;
					}

					for( var g = 0; g < $scope.groups.length; g++ ){
						var group = $scope.groups[g];
						if(!dataTime[group.name]){
							dataTime[group.name] = [];
						}
						dataTime[group.name].push({x:timeCurrent*1000, y:group.total});
						group.total = 0;
					}
				}
				for( var g = 0; g < $scope.groups.length; g++ ){
					var group = $scope.groups[g];
					group.serie = $scope.chart.addSeries({
					    name: group.name,
					    data: dataTime[group.name]
					});
				}
			break;
			case 'INTERVAl':
				$scope.updateSeries( arg );
				$scope.$digest();
			break;
		}
	});
	
	$scope.updateSeries = function( time ){
		for( var i = 0; i < $scope.groups.length; i++ ){
			$scope.groups[i].total = 0;
		}
		if( $rootScope.trunks && $rootScope.trunks.length ){
			for( var i = 0; i < $rootScope.trunks.length; i++ ){
				var trunk = $rootScope.trunks[i];
				var indice = $scope.groupsName.indexOf(trunk.group);
				if( indice == -1 ){
					indice = $scope.groupsName.length;
					$scope.groupsName.push(trunk.group);
					$scope.groups.push({name:trunk.group, total:0, serie:null});
				}
				$scope.groups[indice].total++;
			}
		}
		for( var i = 0; i < $scope.groups.length; i++ ){
			var group = $scope.groups[i];
			if( group.serie ){
				var shift = ($rootScope.config.length<=group.serie.data.length);
				group.serie.addPoint({x:(parseInt(time)*1000), y:group.total}, true, shift);
			} else {
				group.serie = $scope.chart.addSeries({
				    name: group.name,
				    data: {x:(parseInt(time)*1000), y:group.total}
				});
			}
		}
	};
})
.run(function( $rootScope ){

	$rootScope.config = null;
	$rootScope.trunks = [];

	Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

	$rootScope.sockjs = new SockJS('/sockjs');
	$rootScope.sockjs.onopen = function() {
		console.log('open');
	};
	$rootScope.sockjs.onmessage = function(e) {
		var data = angular.fromJson(e.data);
		switch( data.cmd ){
			case 'CONFIG':
				$rootScope.config = data.obj;
				$rootScope.$digest();
			break;
			case 'INIT':
				$rootScope.trunks = data.obj;
				$rootScope.$digest();
			break;
			case 'ADD':
				$rootScope.trunks.push(data.obj);
				$rootScope.$digest();
			break;
			case 'REMOVE':
				$rootScope.trunks.splice(data.obj, 1);
				$rootScope.$digest();
			break;
		}
		$rootScope.$broadcast('SockJS.onMessage', data.cmd, data.obj);
	};
	$rootScope.sockjs.onclose = function() {
		console.log('close');
	};
})
.directive('chart', function () {
  return {
    restrict: 'E',
    scope: {
    	init: '=',
    	chart: '=',
	},
    template: '<div></div>',
    transclude: true,
    replace: true,
    link: function (iScope, iElement, iAttrs, iController) {
    	iScope.$watch('init', function(newValue, oldValue, scope) {
    		if( newValue ) {
        		newValue.chart.renderTo = iElement[0];
	            iScope.chart = new Highcharts.Chart(newValue);
        	}
        	return newValue;
        });
      }
    }

});