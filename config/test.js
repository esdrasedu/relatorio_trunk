var fog_service = JSON.parse(process.env.VCAP_SERVICES);
var redis_service = fog_service["redis-2.2"][0]["credentials"];
var rabbitmq_service = fog_service["rabbitmq-2.4"][0]["credentials"];

var config = {
	amq: {
		connection: {
			port: 		redis_service.port,
			login: 		redis_service.user,
			password: 	redis_service.password,
			vhost: 		redis_service.vhost
		},
		exchange: 		'amq.topic',//Only for test
		queue: {
			name: 		'',
			options: {
				exclusive: true
			}
		},
		routing_key: 	'trunk'
	},
	redis: {
		port: 		rabbitmq_service.port,
		localhost: 	rabbitmq_service.host,
		password: 	rabbitmq_service.password,
		options: 	{

		}
	},
	report: {
		interval: 	6,//3600, //Interval of tick report (based on seconds)
		length: 	10//24 //Size of intervals
	}
};

module.exports = config;