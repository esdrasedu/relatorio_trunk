var config = {
	amq: {
		connection: {
			host: 		'0.0.0.0',
			port: 		5672,
			login: 		'guest',
			password: 	'guest',
			vhost: 		'/',
		},
		exchange: 		'amq.topic',//Only for test
		queue: {
			name: 		'',
			options: {
				exclusive: true
			}
		},
		routing_key: 	'trunk'
	},
	redis: {
		port: 		6379,
		localhost: 	'0.0.0.0',
		options: 	{}
	},
	report: {
		interval: 	6,//3600, //Interval of tick report (based on seconds)
		length: 	10//24 //Size of intervals
	}
}

module.exports = config;